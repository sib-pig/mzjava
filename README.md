# Overview

The Proteome Informatics Group (PIG) at the Swiss Institute of Bioinformatics (SIB) is involved in the development of proteomics applications and services mainly available at [ExPASy](http://www.expasy.ch/) server.

MzJava is an on-going open-source project providing classes for the analysis of mass spectrometry data and enabling rapid bioinformatics application development in the Java programming language.

#Git

To allow modular development the project has been split into separate repositories. The parent repository is [mzjava](https://bitbucket.org/sib-pig/mzjava) and there are separate repositories for [mzjava-core](https://bitbucket.org/sib-pig/mzjava-core), [mzjava-proteomics](https://bitbucket.org/sib-pig/mzjava-proteomics) and [mzjava-glycomics](https://bitbucket.org/sib-pig/mzjava-glycomics). 

To set up the project cd to the directory in which the project is to be cloned and execute the following commands
```
#!bash
git clone https://me@bitbucket.org/sib-pig/mzjava.git                #replacing me with your git user name
cd mzjava
git clone https://me@bitbucket.org/sib-pig/mzjava-core.git           #replacing me with your git user name
git clone https://me@bitbucket.org/sib-pig/mzjava-proteomics.git     #replacing me with your git user name
git clone https://me@bitbucket.org/sib-pig/mzjava-glycomics.git      #replacing me with your git user name
```

# Contribution guidelines

To contribute to the MzJava project follow the [forking workflow](https://www.atlassian.com/git/tutorials/comparing-workflows/forking-workflow). More details can be found on this [page](http://www.techrepublic.com/blog/software-engineer/contribute-to-bitbucket-projects-using-forks-and-pull-requests/) and [video](http://www.youtube.com/watch?v=ssDHUyrQ8nI).

During the review of the pull requests the code will be run through SonarQube. The rules that we use can be found on our public SonarQube server [glyoproteomics.expasy.org](http://glyoproteomics.expasy.org).

To set up forks of the repositories required for MzJava

- Fork the [mzjava](https://bitbucket.org/sib-pig/mzjava) repository by clicking on the Fork button
- Fork the [mzjava-core](https://bitbucket.org/sib-pig/mzjava-core) repository
- Fork the [mzjava-proteomics](https://bitbucket.org/sib-pig/mzjava-proteomics) repository
- Fork the [mzjava-glycomics](https://bitbucket.org/sib-pig/mzjava-glycomics) repository